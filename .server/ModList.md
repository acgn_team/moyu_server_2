# 模组列表-服务器 (WIP)
## 优化
优化游戏体验（资源占用，流畅度，运行效率）
- [氪(forge)](https://www.mcmod.cn/class/5146.html)
- [Lazurite(port of Indium for forge)](https://www.mcmod.cn/class/12701.html)
- [Oculus&GeckoLib兼容](https://www.mcmod.cn/class/13494.html)
- [Oculus&Flywheel兼容](https://www.mcmod.cn/class/7283.html)
- [PacketFixer](https://www.mcmod.cn/class/12625.html)
- [~~铁氧体~~ (内存没节省多少,但是fps大幅下降了...)](https://www.mcmod.cn/class/3888.html)
- [Oculus(iris)](https://www.mcmod.cn/class/5741.html)
- [ModernFix](https://www.mcmod.cn/class/8714.html)
- [starlight](https://www.mcmod.cn/class/3303.html)
- [Canary](https://www.mcmod.cn/class/7598.html)
- [流畅加载-forge](https://www.mcmod.cn/class/6852.html)
- [物理声效](https://www.mcmod.cn/class/8197.html)
- [3D皮肤层](https://www.mcmod.cn/class/4618.html)
- [属性修复](https://www.mcmod.cn/class/2264.html)
- [better fps](https://www.mcmod.cn/class/5242.html)
- [better days](https://www.mcmod.cn/class/11818.html)
- [ImmediatelyFast](https://www.mcmod.cn/class/7948.html)
- [Sinytra互联](https://www.mcmod.cn/class/11627.html)
- [Sinytra互联-额外(Manual built: Removed | reach-entity-attributes)](https://github.com/Sinytra/ConnectorExtras/issues/33)
- [更好的燃烧机制](https://www.mcmod.cn/class/2780.html)
- [警卫村民](https://www.mcmod.cn/class/2686.html)
- [Fast Async World Save](https://www.curseforge.com/minecraft/mc-mods/fast-async-world-save-forge-fabric)
- [简单备份](https://www.mcmod.cn/class/12986.html)
- [内存泄漏修复](https://www.mcmod.cn/class/6593.html)
- [树叶渲染优化](https://www.mcmod.cn/class/4414.html)
- [实体渲染机制优化](https://www.mcmod.cn/class/3629.html)
- [Saturn](https://www.mcmod.cn/class/7722.html)
- [Alternate Current(优化红石)](https://www.mcmod.cn/class/7121.html)
#### Fabric
- [动态环绕](https://www.mcmod.cn/class/1083.html)
- [Continuity(材质连接)](https://www.mcmod.cn/class/4906.html)



## 辅助
信息显示，游戏内帮助，汉化，辅助功能
- [全键无冲](https://www.curseforge.com/minecraft/mc-mods/nonconflictkeys)
- [越肩视角重置](https://www.mcmod.cn/class/3305.html)
- [i18n-汉化](https://www.mcmod.cn/class/1188.html)
- [简单语音聊天](https://www.mcmod.cn/class/3693.html)
- [Journey地图](https://www.mcmod.cn/class/198.html)
- [Journey地图集成](https://www.mcmod.cn/class/4865.html)
- [Jade 玉](https://www.mcmod.cn/class/3482.html)
- [Jade-Addon](https://www.mcmod.cn/class/5837.html)
- [JEI](https://www.mcmod.cn/class/459.html)
- [JER](https://www.mcmod.cn/class/855.html)
- [JEI-拼音搜索](https://www.mcmod.cn/class/840.html)
- [多态合成](https://www.mcmod.cn/class/2895.html)
- [FarSight](https://www.mcmod.cn/class/5224.html)
- [村民卖动物](https://www.mcmod.cn/class/8145.html)
- [被动盾牌](https://www.mcmod.cn/class/8145.html)
- [快速盾牌](https://www.mcmod.cn/class/10865.html)
- [攻击穿草](https://www.mcmod.cn/class/12242.html)
- [鼠标手势](https://www.mcmod.cn/class/1162.html)
- [一键背包整理Next](https://www.mcmod.cn/class/4104.html)
- [quark夸克](https://www.mcmod.cn/class/13572.html)
- [动态光源](https://www.mcmod.cn/class/5302.html)


## 玩法
整合包核心，内容
### 建筑
- [美化!](https://www.mcmod.cn/class/7263.html)
- [精巧手艺](https://www.mcmod.cn/class/9261.html)
- [锦致装饰](https://www.mcmod.cn/class/3555.html)
- [MrCrayfish 的家具](https://www.mcmod.cn/class/263.html)
### 冒险&探索
- [暮色森林](https://www.mcmod.cn/class/61.html)
- [灾变](https://www.mcmod.cn/class/5214.html)
- [照相机](https://www.mcmod.cn/class/2848.html)
- [治愈营火](https://www.mcmod.cn/class/2092.html)
- [地牢浮现时](https://www.mcmod.cn/class/3607.html)
- [地牢爬行](https://www.mcmod.cn/class/3105.html)
- [Towns and Towers](https://www.mcmod.cn/class/7000.html)
- [地下城武器](https://www.mcmod.cn/class/11950.html)
- [探险者指南针](https://www.mcmod.cn/class/4395.html)
- [生物群戏指南针](https://www.mcmod.cn/class/754.html)
- [Integrated Dungeons and Structures](https://www.mcmod.cn/class/6142.html)
### 畜牧渔业(摸鱼)
- [农夫乐事](https://www.mcmod.cn/class/2820.html)
- [农夫乐事+AlexMobs](https://www.mcmod.cn/class/5565.html)
- [Delightful](https://www.mcmod.cn/class/6817.html)
- [料理乐事](https://www.mcmod.cn/class/11405.html)
- [随意乐事](https://www.mcmod.cn/class/11548.html)
- [果园乐事](https://www.mcmod.cn/class/12920.html)
- [农夫乐事-烹饪书](https://github.com/Hotakus/fd-cookbook-reforged)
- [水产业2/水产品2](https://www.mcmod.cn/class/281.html)
- [Aquaculture Delight (A Farmer's Delight Add-on)](https://www.mcmod.cn/class/13332.html)
- [车万女仆](https://www.mcmod.cn/class/1796.html)
### create&瓦尔基里
- [create](https://www.mcmod.cn/class/2021.html)
- [瓦尔基里天空/瓦尔基里战争](https://www.curseforge.com/minecraft/mc-mods/valkyrien-skies)
- [Clockwork: Create x Valkyrien Skies](https://www.mcmod.cn/class/13550.html)
- [Create: Interactive](https://www.mcmod.cn/class/13218.html)
- [Create Crafts & Additions](https://www.mcmod.cn/class/3437.html)
- [Create Slice & Dice](https://www.mcmod.cn/class/7328.html)
- [机械动力：汽鸣铁道](https://www.mcmod.cn/class/8230.html)
- [机械动力：火炮](https://www.mcmod.cn/class/7178.html)
- [Create: Interiors](https://www.mcmod.cn/class/11960.html)
- [Create: New Age](https://www.mcmod.cn/class/11918.html)
- [机械动力：喷气背包](https://www.mcmod.cn/class/7338.html)
- [Eureka! Ships! for Valkyrien Skies](https://www.mcmod.cn/class/8007.html)
- [Kontraption: valkyrien-skies x mekanism](https://modrinth.com/mod/kontraption)
### 工业
#### Mekanism
- [通用机械](https://www.mcmod.cn/class/187.html)
- [通用机械发电机](https://www.mcmod.cn/class/1323.html)
- [通用机械工具](https://www.mcmod.cn/class/1615.html)
- [Just Enough Mekanism Multiblocks](https://www.mcmod.cn/class/12565.html)
#### AE2
- [应用能源：通用机械附属](https://www.mcmod.cn/class/6055.html)
- [应用能源2](https://www.mcmod.cn/class/260.html)
- [MEGA存储单元](https://www.mcmod.cn/class/6792.html)
- [AEInfinityBooster](https://www.mcmod.cn/class/7173.html)
- [AE2无线终端](https://www.mcmod.cn/class/3712.html)
- [AE2扩展](https://www.mcmod.cn/class/11534.html)
#### CC
- [CC: Tweaked](https://www.mcmod.cn/class/1681.html)
- [CC: VS](https://www.mcmod.cn/class/13226.html)
- [CC:C Bridge](https://www.mcmod.cn/class/8487.html)
- [Advanced Peripherals](https://www.mcmod.cn/class/4973.html)
##### 散装科技mod
- [极限反应堆](https://www.mcmod.cn/class/814.html)
- [通量网络](https://www.mcmod.cn/class/803.html)
- [Item Collectors](https://www.mcmod.cn/class/4701.html)
- [Hostile Neural Networks(深度怪物学习-高版本)](https://www.mcmod.cn/class/5461.html)
- [安全工艺](https://www.mcmod.cn/class/443.html)
- [EnderChests](https://www.mcmod.cn/class/10948.html)
- [建筑小帮手](https://www.mcmod.cn/class/1379.html)
- [Scannable扫描仪](https://www.mcmod.cn/class/791.html)


## 地形&世界
世界不在单调生物群系，遗迹，新的维度
#### 世界生成器
- [ReTerraForged](https://github.com/racoonman2/ReTerraForged/tree/1.20.1/forge)
- [村庄革新-ctov](https://www.mcmod.cn/class/6943.html)
- [YUNG-矿井](https://www.mcmod.cn/class/2788.html)
- [YUNG-下届要塞](https://www.mcmod.cn/class/9384.html)
- [YUNG-地牢](https://www.mcmod.cn/class/4429.html)
- [YUNG-海底神殿](https://www.mcmod.cn/class/7904.html)
- [YUNG-沙漠神殿](https://www.mcmod.cn/class/6613.html)
- [YUNG-丛林神殿](https://www.mcmod.cn/class/12060.html)
- [YUNG-女巫小屋](https://www.mcmod.cn/class/6618.html)
- [YUNG-额外](https://www.mcmod.cn/class/4276.html)
- [传送石碑/指路石](https://www.mcmod.cn/class/1339.html)
#### 生物群系
- [wwee](https://www.mcmod.cn/class/4595.html)
- [Nyctophobia](https://modrinth.com/mod/nyctophobia)
- [超多生物群系](https://www.mcmod.cn/class/108.html)
- [Geophilic Reforged](https://modrinth.com/mod/geophilic-reforged)
#### 动植物
- [Alex 的生物](https://www.mcmod.cn/class/3318.html)
- [丰富的生态](https://www.mcmod.cn/class/6198.html)
#### 游戏难度
- [神化](https://www.mcmod.cn/class/1708.html)
- [Apotheotic Additions](https://www.mcmod.cn/class/14269.html)
- [月亮事件](https://www.mcmod.cn/class/3452.html)
#### Fabric
- [更好的下界](https://www.mcmod.cn/class/1579.html)
- [更好的末地](https://www.mcmod.cn/class/3163.html)



## 美化
UI美化，人性化等
- [聊天头像](https://www.mcmod.cn/class/4523.html)
- [史蒂夫模型](https://www.mcmod.cn/class/8616.html)
- [ModernUI](https://www.mcmod.cn/class/2454.html)
- [聊天气泡](https://www.mcmod.cn/class/5218.html)
- [沉浸画框](https://www.mcmod.cn/class/6947.html)
- [苹果皮](https://www.mcmod.cn/class/744.html)
- [垃圾槽](https://www.mcmod.cn/class/1893.html)
- [RPG-HUD](https://www.mcmod.cn/class/5711.html)
- [物品缩放](https://www.mcmod.cn/class/3048.html)
- [自然环境](https://www.mcmod.cn/class/2947.html)
- [额外的声音](https://www.mcmod.cn/class/12944.html)
- [物品栏HUD+](https://www.mcmod.cn/class/3395.html)
- [键位冲突显示](https://www.mcmod.cn/class/1191.html)
- [Memory Usage Title](https://www.mcmod.cn/class/13732.html)
#### Fabric
- [耐久信息显示](https://www.mcmod.cn/class/2653.html)


## Lib
Mod dependencies
- [Forgified Fabric API](https://www.mcmod.cn/class/11464.html)
- [bad packets](https://www.mcmod.cn/class/6275.html)
- [Balm](https://www.mcmod.cn/class/4485.html)
- [Curios API](https://www.mcmod.cn/class/2029.html)
- [Citadel](https://www.mcmod.cn/class/2232.html)
- [Collective](https://www.mcmod.cn/class/2848.html)
- [CreativeCore](https://www.mcmod.cn/class/1283.html)
- [Cupboard](https://www.mcmod.cn/class/11562.html)
- [Cristel Lib](https://www.mcmod.cn/class/12373.html)
- [GeckoLib](https://www.mcmod.cn/class/3232.html)
- [Cloth Config API (Fabric/Forge/NeoForge)](https://www.mcmod.cn/class/2346.html)
- [TerraBlender](https://www.mcmod.cn/class/5489.html)
- [Lithostitched](https://www.curseforge.com/minecraft/mc-mods/lithostitched)
- [YUNG's API](https://www.mcmod.cn/class/3372.html)
- [Resourceful Lib](https://www.mcmod.cn/class/7647.html)
- [Placebo](https://www.mcmod.cn/class/1023.html)
- [帕秋莉手册](https://www.mcmod.cn/class/1388.html)
- [Apothic Attributes (AttributesLib)](https://www.mcmod.cn/class/12036.html)
- [CorgiLib](https://www.mcmod.cn/class/8018.html)
- [Resourceful Lib](https://www.mcmod.cn/class/7647.html)
- [Kotlin for Forge](https://www.mcmod.cn/class/2890.html)
- [Architectury API (Fabric/Forge/NeoForge)](https://www.mcmod.cn/class/3434.html)
- [Searchables](https://www.mcmod.cn/class/11102.html)
- [犀牛](https://www.mcmod.cn/class/3821.html)
- [Moonlight Lib](https://www.mcmod.cn/class/4159.html)
- [MidnightLib](https://www.mcmod.cn/class/6776.html)
- [Fusion (Connected Textures)](https://www.mcmod.cn/class/11194.html)
- [SuperMartijn642's Config Lib](https://www.mcmod.cn/class/4682.html)
- [Glodium](https://www.mcmod.cn/class/13307.html)
- [libIPN](https://www.mcmod.cn/class/7713.html)
- [Botarium](https://www.mcmod.cn/class/8235.html)
- [Zeta](https://www.mcmod.cn/class/13572.html)
- [Integrated API](https://modrinth.com/mod/integrated-api)
- [ShetiPhianCore](https://www.mcmod.cn/class/789.html)
- [ZeroCore 2](https://www.curseforge.com/minecraft/mc-mods/zerocore)
#### Fabric
- [BCLib](https://www.mcmod.cn/class/4183.html)



## 魔改
额
- [kubejs](https://modrinth.com/mod/kubejs)
- [kubejs-create](https://www.mcmod.cn/class/5157.html)
- [ProbeJS](https://www.mcmod.cn/class/6486.html)


## 开发
for easier development
- [Forge Config Screens](https://www.mcmod.cn/class/5220.html)
- [Async Locator](https://www.mcmod.cn/class/8544.html)
- [游戏菜单模组设定](https://www.mcmod.cn/class/3805.html)
- [怪物数量控制](https://www.curseforge.com/minecraft/mc-mods/spawncapcontrolutility)
- [Spark](https://www.mcmod.cn/class/4073.html)
- [可视性能侦测](https://www.mcmod.cn/class/4832.html)